//
//  BaseService.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class BaseService: NSObject {

    func enrichUrl(rawUrl: String) -> String {
        let enrichedUrl = String(format: "%@?api_key=%@", rawUrl, Constants.TMDB_API_KEY)
        return enrichedUrl
    }

    func enrichGetParameters(rawUrl: String, params: [[String: String]]) -> String {
        var resultUrl = rawUrl
        if var urlComponents = URLComponents(string: rawUrl) {
            urlComponents.query = nil
            resultUrl = urlComponents.url?.absoluteString ?? rawUrl
        }
        for (index, param) in params.enumerated() {
            if index == 0 {
                resultUrl.append("?")
            } else {
                resultUrl.append("&")
            }
            resultUrl.append(String(format: "%@=%@", param["key"]!, param["value"]!))
        }
        return resultUrl
    }

    func buildGetRequest(url: URL) -> NSMutableURLRequest {
        let req = NSMutableURLRequest(url: url)
        return req
    }

    func buildPostRequest(url: URL, data: Data?) -> NSMutableURLRequest {
        let req = NSMutableURLRequest(url: url)
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        req.httpMethod = "POST"
        req.httpBody = data
        return req
    }

}
