//
//  LoginService.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 11.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class LoginService: BaseService {

    func readToken(closure: @escaping (String?, ErrorContainer?) -> Void) {
        let urlPath = Constants.BASE_SERVICE_URL + "/authentication/token/new"
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        URLSession.shared.dataTask(with: self.buildGetRequest(url: endpoint) as URLRequest, completionHandler: { (data, response, error) in
            do {
                if response == nil {
                    throw ErrorContainer.noConn
                }
                guard let data = data else {
                    throw ErrorContainer.noData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                    throw ErrorContainer.wrongFormat
                }
                print(json)
                
                let resultDict = json as [String: Any]
                let result: String = resultDict["request_token"] as! String
                closure(result, nil)
            } catch let error as ErrorContainer {
                print(error.rawValue)
                closure(nil, error)
            } catch let error as NSError {
                print(error.debugDescription)
                closure(nil, .generalError)
            }
        }) .resume()
    }
    
    func validateRequestToken(username: String, password: String, closure: @escaping (String?, ErrorContainer?) -> Void) {
        let urlPath = Constants.BASE_SERVICE_URL + "/authentication/token/validate_with_login"
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])

        var dict = [String: Any]()
        dict["username"] = username
        dict["password"] = password
        dict["request_token"] = AppSession.sharedInstance.requestToken ?? ""

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        do {
             let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
             URLSession.shared.dataTask(with: self.buildPostRequest(url: endpoint, data: jsonData) as URLRequest, completionHandler: { (data, response, error) in
                 do {
                     if response == nil {
                         throw ErrorContainer.noConn
                     }
                     guard let data = data else {
                         throw ErrorContainer.noData
                     }
                     guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                         throw ErrorContainer.wrongFormat
                     }
                     print(json)
                     
                    let resultDict = json as [String: Any]
                    if (resultDict["status_message"] as? String) != nil {
                        closure(nil, ErrorContainer.authError)
                    } else {
                        if let reqToken = resultDict["request_token"] as? String {
                            closure(reqToken, nil)
                        } else {
                            closure(nil, ErrorContainer.authError)
                        }
                    }
                 } catch let error as ErrorContainer {
                     print(error.rawValue)
                     closure(nil, error)
                 } catch let error as NSError {
                     print(error.debugDescription)
                     closure(nil, .generalError)
                 }
             }) .resume()
        } catch {
            closure(nil, .generalError)
        }
    }

    func createNewSession(closure: @escaping (String?, ErrorContainer?) -> Void) {
        let urlPath = Constants.BASE_SERVICE_URL + "/authentication/session/new"
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])

        var dict = [String: Any]()
        dict["request_token"] = AppSession.sharedInstance.requestToken ?? ""

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        do {
             let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
             URLSession.shared.dataTask(with: self.buildPostRequest(url: endpoint, data: jsonData) as URLRequest, completionHandler: { (data, response, error) in
                 do {
                     if response == nil {
                         throw ErrorContainer.noConn
                     }
                     guard let data = data else {
                         throw ErrorContainer.noData
                     }
                     guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                         throw ErrorContainer.wrongFormat
                     }
                     print(json)
                     
                    let resultDict = json as [String: Any]
                    if (resultDict["status_message"] as? String) != nil {
                        closure(nil, ErrorContainer.apiError)
                    } else {
                        if let sessionId = resultDict["session_id"] as? String {
                            closure(sessionId, nil)
                        } else {
                            closure(nil, ErrorContainer.authError)
                        }
                    }
                 } catch let error as ErrorContainer {
                     print(error.rawValue)
                     closure(nil, error)
                 } catch let error as NSError {
                     print(error.debugDescription)
                     closure(nil, .generalError)
                 }
             }) .resume()
        } catch {
            closure(nil, .generalError)
        }
    }
    
}
