//
//  MovieService.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class MovieService: BaseService {

    func readMovieList(keyword: String, closure: @escaping ([Movie]?, ErrorContainer?) -> Void) {
        let urlPath = Constants.BASE_SERVICE_URL + "/discover/movie"
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])
        if keyword.count > 0 {
            params.append(["key": "with_keywords", "value": keyword])
        }

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        URLSession.shared.dataTask(with: self.buildGetRequest(url: endpoint) as URLRequest, completionHandler: { (data, response, error) in
            do {
                if response == nil {
                    throw ErrorContainer.noConn
                }
                guard let data = data else {
                    throw ErrorContainer.noData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                    throw ErrorContainer.wrongFormat
                }
                print(json)
                
                var result: [Movie] = []
                let resultsArr = json["results"] as! [[String: Any]]
                for movieDict in resultsArr {
                    result.append(Movie(movieDict as NSDictionary))
                }
                closure(result, nil)
            } catch let error as ErrorContainer {
                print(error.rawValue)
                closure(nil, error)
            } catch let error as NSError {
                print(error.debugDescription)
                closure(nil, .generalError)
            }
        }) .resume()
    }
    
    func readMovieDetail(movieId: Int, closure: @escaping (Movie?, ErrorContainer?) -> Void) {
        let urlPath = String(format: Constants.BASE_SERVICE_URL + "/movie/%d", movieId)
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        URLSession.shared.dataTask(with: self.buildGetRequest(url: endpoint) as URLRequest, completionHandler: { (data, response, error) in
            do {
                if response == nil {
                    throw ErrorContainer.noConn
                }
                guard let data = data else {
                    throw ErrorContainer.noData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                    throw ErrorContainer.wrongFormat
                }
                print(json)
                
                let resultDict = json as [String: Any]
                let result = Movie(resultDict as NSDictionary)
                closure(result, nil)
            } catch let error as ErrorContainer {
                print(error.rawValue)
                closure(nil, error)
            } catch let error as NSError {
                print(error.debugDescription)
                closure(nil, .generalError)
            }
        }) .resume()
    }
    
}
