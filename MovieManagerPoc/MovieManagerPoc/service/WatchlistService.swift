//
//  WatchlistService.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 11.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class WatchlistService: BaseService {

    func createList(closure: @escaping (Int?, ErrorContainer?) -> Void) {
        let urlPath = Constants.BASE_SERVICE_URL + "/list"
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])
        params.append(["key": "session_id", "value": Constants.SESSION_ID_USERDEFAULTS_KEY])

        var dict = [String: Any]()
        dict["name"] = "watchlist"

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        do {
             let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
             URLSession.shared.dataTask(with: self.buildPostRequest(url: endpoint, data: jsonData) as URLRequest, completionHandler: { (data, response, error) in
                 do {
                     if response == nil {
                         throw ErrorContainer.noConn
                     }
                     guard let data = data else {
                         throw ErrorContainer.noData
                     }
                     guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                         throw ErrorContainer.wrongFormat
                     }
                     print(json)
                     
                    let resultDict = json as [String: Any]
                    let successFlag = resultDict["success"] as? Bool
                    if successFlag ?? false {
                        let listId = resultDict["list_id"] as? Int
                        closure(listId, nil)
                    } else {
                        closure(nil, ErrorContainer.apiError)
                    }
                 } catch let error as ErrorContainer {
                     print(error.rawValue)
                     closure(nil, error)
                 } catch let error as NSError {
                     print(error.debugDescription)
                     closure(nil, .generalError)
                 }
             }) .resume()
        } catch {
            closure(nil, .generalError)
        }
    }
    
    func readWatchlist(closure: @escaping ([Movie]?, ErrorContainer?) -> Void) {
        let urlPath = String(format: Constants.BASE_SERVICE_URL + "/list/%d", AppSession.sharedInstance.tmdbWatchlistId ?? 0)
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        URLSession.shared.dataTask(with: self.buildGetRequest(url: endpoint) as URLRequest, completionHandler: { (data, response, error) in
            do {
                if response == nil {
                    throw ErrorContainer.noConn
                }
                guard let data = data else {
                    throw ErrorContainer.noData
                }
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                    throw ErrorContainer.wrongFormat
                }
                print(json)
                
                var result: [Movie] = []
                let resultArr = json["items"] as! [[String: Any]]
                for movieDict in resultArr {
                    result.append(Movie(movieDict as NSDictionary))
                }
                closure(result, nil)
            } catch let error as ErrorContainer {
                print(error.rawValue)
                closure(nil, error)
            } catch let error as NSError {
                print(error.debugDescription)
                closure(nil, .generalError)
            }
        }) .resume()
    }
    
    func addToList(movieId: Int, closure: @escaping (ErrorContainer?) -> Void) {
        let urlPath = String(format: Constants.BASE_SERVICE_URL + "/list/%d/add_item", AppSession.sharedInstance.tmdbWatchlistId ?? 0)
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])
        params.append(["key": "session_id", "value": Constants.SESSION_ID_USERDEFAULTS_KEY])

        var dict = [String: Any]()
        dict["media_id"] = movieId

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        do {
             let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
             URLSession.shared.dataTask(with: self.buildPostRequest(url: endpoint, data: jsonData) as URLRequest, completionHandler: { (data, response, error) in
                 do {
                     if response == nil {
                         throw ErrorContainer.noConn
                     }
                     guard let data = data else {
                         throw ErrorContainer.noData
                     }
                     guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                         throw ErrorContainer.wrongFormat
                     }
                     print(json)
                     
                    let resultDict = json as [String: Any]
                    let statusCode = resultDict["status_code"] as? Int
                    if statusCode == 12 {
                        closure(nil)
                    } else {
                        closure(ErrorContainer.apiError)
                    }
                 } catch let error as ErrorContainer {
                     print(error.rawValue)
                     closure(error)
                 } catch let error as NSError {
                     print(error.debugDescription)
                     closure(.generalError)
                 }
             }) .resume()
        } catch {
            closure(.generalError)
        }
    }

    func removeFromList(movieId: Int, closure: @escaping (ErrorContainer?) -> Void) {
        let urlPath = String(format: Constants.BASE_SERVICE_URL + "/list/%d/remove_item", AppSession.sharedInstance.tmdbWatchlistId ?? 0)
        
        var params: [[String: String]] = []
        params.append(["key": "api_key", "value": Constants.TMDB_API_KEY])
        params.append(["key": "session_id", "value": Constants.SESSION_ID_USERDEFAULTS_KEY])

        var dict = [String: Any]()
        dict["media_id"] = movieId

        let finalPath = self.enrichGetParameters(rawUrl: urlPath, params: params)
        guard let endpoint = URL(string: finalPath) else {
            print("General error occured")
            return
        }

        do {
             let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
             URLSession.shared.dataTask(with: self.buildPostRequest(url: endpoint, data: jsonData) as URLRequest, completionHandler: { (data, response, error) in
                 do {
                     if response == nil {
                         throw ErrorContainer.noConn
                     }
                     guard let data = data else {
                         throw ErrorContainer.noData
                     }
                     guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, AnyObject> else {
                         throw ErrorContainer.wrongFormat
                     }
                     print(json)
                     
                    let resultDict = json as [String: Any]
                    let statusCode = resultDict["status_code"] as? Int
                    if statusCode == 13 {
                        closure(nil)
                    } else {
                        closure(ErrorContainer.apiError)
                    }
                 } catch let error as ErrorContainer {
                     print(error.rawValue)
                     closure(error)
                 } catch let error as NSError {
                     print(error.debugDescription)
                     closure(.generalError)
                 }
             }) .resume()
        } catch {
            closure(.generalError)
        }
    }
}
