//
//  SearchController.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class SearchController: BaseController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet var mainTable: UITableView!

    let movieService = MovieService()
    var movieList: [Movie] = []
    var filteredMovieList: [Movie] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainTable.register(UINib(nibName: "SearchResultCell", bundle: nil), forCellReuseIdentifier: "SearchResultCell")
        
        self.showLoading()
        self.movieService.readMovieList(keyword: "") { (list, error) in
            self.hideLoading()
            if error != nil {
                self.showMessage(message: error!.rawValue)
            } else {
                if list != nil {
                    self.movieList = list!
                    self.filteredMovieList = list!
                    DispatchQueue.main.async {
                        self.mainTable.reloadData()
                    }
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredMovieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let movie = self.filteredMovieList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as! SearchResultCell
        cell.loadData(movie: movie)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.filteredMovieList[indexPath.row]
        self.presentDetail(for: movie)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    @IBAction func logoutClicked() {
        DispatchQueue.main.async {
            let loginController = self.storyboard!.instantiateViewController(withIdentifier: "Login") as! LoginController
            self.present(loginController, animated: false, completion: nil)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            self.filteredMovieList = self.movieList.filter({ (movie) -> Bool in
                (movie.title?.contains(searchText) ?? false)
            })
        } else {
            self.filteredMovieList = self.movieList
        }
        DispatchQueue.main.async {
            self.mainTable.reloadData()
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
