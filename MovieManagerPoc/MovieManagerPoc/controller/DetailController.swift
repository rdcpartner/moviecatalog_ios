//
//  DetailController.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit
import Kingfisher

class DetailController: BaseController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var contentLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var ratingLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var watchlistButton: UIBarButtonItem!
    @IBOutlet var favButton: UIBarButtonItem!

    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func loadMovie(movieRef: Movie) {
        self.movie = movieRef
        self.contentLabel.text = movieRef.overview
        self.ratingLabel.text = String(format: "%.1f", movieRef.voteAverage ?? 0.0)
        self.titleLabel.text = movieRef.originalTitle ?? ""
        self.dateLabel.text = movieRef.releaseDate ?? ""
        if let imgPath = movieRef.backdropPath {
            self.imageView.kf.setImage(with: URL(string: imgPath))
        }
        self.revisitTint(for: .watchList)
        self.revisitTint(for: .favorite)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.movie != nil {
            self.revisitTint(for: .watchList)
            self.revisitTint(for: .favorite)
        }
    }

    @IBAction func watchlistClicked() {
        if ClientStoreWrapper.sharedInstance.checkIfAlreadyAdded(movie: self.movie!, type: .watchList) {
            ClientStoreWrapper.sharedInstance.deleteMovie(movie: self.movie!, type: .watchList)
        } else {
            ClientStoreWrapper.sharedInstance.insertMovie(movie: self.movie!, type: .watchList)
        }
        self.revisitTint(for: .watchList)
    }
    
    @IBAction func favoriteClicked() {
        if ClientStoreWrapper.sharedInstance.checkIfAlreadyAdded(movie: self.movie!, type: .favorite) {
            ClientStoreWrapper.sharedInstance.deleteMovie(movie: self.movie!, type: .favorite)
        } else {
            ClientStoreWrapper.sharedInstance.insertMovie(movie: self.movie!, type: .favorite)
        }
        self.revisitTint(for: .favorite)
    }
    
    func revisitTint(for type: WrapperType) {
        switch type {
        case .watchList:
            let activeFlag = ClientStoreWrapper.sharedInstance.checkIfAlreadyAdded(movie: self.movie!, type: .watchList)
            self.watchlistButton.tintColor = activeFlag ? UIColor.green : UIColor.darkGray
        case .favorite:
            let activeFlag = ClientStoreWrapper.sharedInstance.checkIfAlreadyAdded(movie: self.movie!, type: .favorite)
            self.favButton.tintColor = activeFlag ? UIColor.green : UIColor.darkGray
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
