//
//  BaseController.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator

class BaseController: UIViewController {

    let activityIndicator = MDCActivityIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.sizeToFit()
        activityIndicator.cycleColors = [UIColor.white]
        activityIndicator.center = self.view.center
        activityIndicator.isHidden = true
        view.addSubview(activityIndicator)
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.view.bringSubviewToFront(self.activityIndicator)
            self.activityIndicator.isHidden = false
            self.activityIndicator.startAnimating()
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            self.activityIndicator.isHidden = true
            self.activityIndicator.stopAnimating()
        }
    }

    func showMessage(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
        }
    }

    func presentDetail(for movie: Movie) {
        DispatchQueue.main.async {
            let detailController = self.storyboard!.instantiateViewController(withIdentifier: "MovieDetail") as! DetailController
            print(detailController.view)
            self.navigationController?.pushViewController(detailController, animated: true)
            detailController.loadMovie(movieRef: movie)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
