//
//  LoginController.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class LoginController: BaseController {

    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!

    let loginService = LoginService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func loginClicked() {
        let emailVal = self.emailField.text
        let passVal = self.passwordField.text
        if emailVal?.count == 0 || passVal?.count == 0 {
            self.showMessage(message: Constants.LOGIN_AUTH_ERROR)
            return
        }
        self.loginService.readToken { (reqToken, error) in
            if error != nil {
                self.showMessage(message: error!.rawValue)
            } else {
                self.loginService.validateRequestToken(username: emailVal!, password: passVal!) { (finalToken, loginError) in
                    if loginError != nil {
                        self.showMessage(message: loginError!.rawValue)
                    } else {
                        AppSession.sharedInstance.requestToken = finalToken
                        self.loginService.createNewSession { (sessionId, error) in
                            if sessionId != nil {
                                AppSession.sharedInstance.sessionId = sessionId
                            }
                        }
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "MainSegue", sender: nil)
                        }
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
