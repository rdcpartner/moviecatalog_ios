//
//  WrapperType.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 10.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

enum WrapperType: Int {

    case watchList = 0
    case favorite = 1
    
}
