//
//  Movie.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class Movie: NSObject, NSCoding {

    var movieId: Int?
    var backdropPath: String?
    var originalTitle: String?
    var overview: String?
    var posterPath: String?
    var releaseDate: String?
    var title: String?
    var voteAverage: Double?
    var voteCount: Int?
    
    override init() {}

    init(_ dict : NSDictionary) {
        if let movieId = dict["id"] as? Int {
            self.movieId = movieId
        }
        if let backdropPath = dict["backdrop_path"] as? String {
            self.backdropPath = String(format: "%@%@", Constants.TMDB_IMAGE_PREFIX, backdropPath)
        }
        if let originalTitle = dict["original_title"] as? String {
            self.originalTitle = originalTitle
        }
        if let overview = dict["overview"] as? String {
            self.overview = overview
        }
        if let posterPath = dict["poster_path"] as? String {
            self.posterPath = String(format: "%@%@", Constants.TMDB_IMAGE_PREFIX, posterPath)
        }
        if let releaseDate = dict["release_date"] as? String {
            self.releaseDate = releaseDate
        }
        if let title = dict["title"] as? String {
            self.title = title
        }
        if let voteAverage = dict["vote_average"] as? Double {
            self.voteAverage = voteAverage
        }
        if let voteCount = dict["vote_count"] as? Int {
            self.voteCount = voteCount
        }
    }

    required convenience init(coder aDecoder: NSCoder) {
        self.init()
        self.movieId = aDecoder.decodeInteger(forKey: "movieId")
        self.backdropPath = aDecoder.decodeObject(forKey: "backdropPath") as? String
        self.originalTitle = aDecoder.decodeObject(forKey: "originalTitle") as? String
        self.overview = aDecoder.decodeObject(forKey: "overview") as? String
        self.posterPath = aDecoder.decodeObject(forKey: "posterPath") as? String
        self.releaseDate = aDecoder.decodeObject(forKey: "releaseDate") as? String
        self.title = aDecoder.decodeObject(forKey: "title") as? String
        self.voteAverage = aDecoder.decodeDouble(forKey: "voteAverage")
        self.voteCount = aDecoder.decodeInteger(forKey: "voteCount")
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(movieId!, forKey: "movieId")
        aCoder.encode(backdropPath, forKey: "backdropPath")
        aCoder.encode(originalTitle, forKey: "originalTitle")
        aCoder.encode(overview, forKey: "overview")
        aCoder.encode(posterPath, forKey: "posterPath")
        aCoder.encode(releaseDate, forKey: "releaseDate")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(voteAverage!, forKey: "voteAverage")
        aCoder.encode(voteCount!, forKey: "voteCount")
    }
    
}
