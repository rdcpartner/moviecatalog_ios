//
//  UIView+Extension.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
}
