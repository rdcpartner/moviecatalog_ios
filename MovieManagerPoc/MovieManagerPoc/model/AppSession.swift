//
//  AppSession.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class AppSession: NSObject {

    static let sharedInstance = AppSession()

    var requestToken: String? {
        get {
            return UserDefaults.standard.string(forKey: Constants.REQUEST_TOKEN_USERDEFAULTS_KEY)
        }
        set(newVal) {
            UserDefaults.standard.setValue(newVal, forKey: Constants.REQUEST_TOKEN_USERDEFAULTS_KEY)
            UserDefaults.standard.synchronize()
        }
    }

    var sessionId: String? {
        get {
            return UserDefaults.standard.string(forKey: Constants.SESSION_ID_USERDEFAULTS_KEY)
        }
        set(newVal) {
            UserDefaults.standard.setValue(newVal, forKey: Constants.SESSION_ID_USERDEFAULTS_KEY)
            UserDefaults.standard.synchronize()
        }
    }

    var tmdbWatchlistId: Int? {
        get {
            return UserDefaults.standard.integer(forKey: Constants.TMDB_WATCHLIST_ID_KEY)
        }
        set(newVal) {
            UserDefaults.standard.setValue(newVal, forKey: Constants.TMDB_WATCHLIST_ID_KEY)
            UserDefaults.standard.synchronize()
        }
    }

}
