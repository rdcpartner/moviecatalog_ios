//
//  ErrorContainer.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

enum ErrorContainer: String, Error {
    case noData = "No data"
    case noConn = "Please check your internet connection"
    case wrongFormat = "Wrong format"
    case apiError = "An API error occured"
    case generalError = "A general error occured"
    case authError = "Authenication error"
}
