//
//  BaseMovieCell.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 10.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class BaseMovieCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var posterImgView: UIImageView!

    var movie: Movie?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadData(movieRef: Movie) {
        self.movie = movieRef
        self.titleLabel.text = movieRef.title ?? movieRef.originalTitle ?? ""
        self.dateLabel.text = movieRef.releaseDate ?? ""
        if let imgPath = movieRef.posterPath {
            self.posterImgView.kf.setImage(with: URL(string: imgPath))
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
