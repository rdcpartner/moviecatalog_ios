//
//  SearchResultCell.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit
import Kingfisher

class SearchResultCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var rateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func loadData(movie: Movie) {
        self.titleLabel.text = movie.title ?? movie.originalTitle ?? ""
        self.dateLabel.text = movie.releaseDate ?? ""
        self.rateLabel.text = String(format: "%.1f", movie.voteAverage ?? 0.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
