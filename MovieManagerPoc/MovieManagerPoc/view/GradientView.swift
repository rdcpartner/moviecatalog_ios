//
//  GradientView.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {

    @IBInspectable var startColor: UIColor = UIColor.clear
    @IBInspectable var endColor: UIColor = UIColor.clear
    
    var gradient: CAGradientLayer?
    
    override func draw(_ rect: CGRect) {
        if gradient != nil {
            gradient?.removeFromSuperlayer()
        }
        
        gradient = CAGradientLayer()
        gradient!.frame = CGRect(x: CGFloat(0),
                                y: CGFloat(0),
                                width: self.frame.size.width,
                                height: self.frame.size.height)
        gradient!.colors = [startColor.cgColor, endColor.cgColor]
        gradient!.zPosition = -1
        layer.addSublayer(gradient!)
    }

}
