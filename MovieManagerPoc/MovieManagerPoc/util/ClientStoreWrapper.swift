//
//  ClientStoreWrapper.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 10.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class ClientStoreWrapper: NSObject {

    static let sharedInstance = ClientStoreWrapper()

    func keyForWrapperType(type: WrapperType) -> String {
        switch type {
        case .watchList:
            return Constants.WATCHLIST_USERDEFAULTS_KEY
        case .favorite:
            return Constants.FAVORITE_USERDEFAULTS_KEY
        }
    }

    func insertMovie(movie: Movie, type: WrapperType) {
        if !self.checkIfAlreadyAdded(movie: movie, type: type) {
            var currentList = self.readMovies(type: type)
            currentList.append(movie)
            let defaults = UserDefaults.standard
            let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
            defaults.set(data, forKey: self.keyForWrapperType(type: type))
            defaults.synchronize()
        }
    }

    func deleteMovie(movie: Movie, type: WrapperType) {
        if self.checkIfAlreadyAdded(movie: movie, type: type) {
            var currentList = self.readMovies(type: type)
            currentList.removeAll { (item) -> Bool in
                item.movieId == movie.movieId
            }
            let defaults = UserDefaults.standard
            let data = NSKeyedArchiver.archivedData(withRootObject: currentList)
            defaults.set(data, forKey: self.keyForWrapperType(type: type))
            defaults.synchronize()
        }
    }

    func readMovies(type: WrapperType) -> [Movie] {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: self.keyForWrapperType(type: type)) as? NSData {
            let resultList = NSKeyedUnarchiver.unarchiveObject(with: data as Data) as! [Movie]
            return resultList
        }
        return [Movie]()
    }
    
    func checkIfAlreadyAdded(movie: Movie, type: WrapperType) -> Bool {
        let currentList = self.readMovies(type: type)
        return currentList.filter { (item) -> Bool in
            item.movieId == movie.movieId
        }.count > 0
    }
}
