//
//  Constants.swift
//  MovieManagerPoc
//
//  Created by mahir tarlan on 9.04.2021.
//  Copyright © 2021 rdc. All rights reserved.
//

import UIKit

class Constants: NSObject {

    static let BASE_SERVICE_URL = "https://api.themoviedb.org/3"
    static let TMDB_API_KEY = "96bedc38d8d7396becc92f7dd29ec692"
    static let TMDB_IMAGE_PREFIX = "https://image.tmdb.org/t/p/w500/"

    static let REQUEST_TOKEN_USERDEFAULTS_KEY = "REQUEST_TOKEN_USERDEFAULTS_KEY"
    static let SESSION_ID_USERDEFAULTS_KEY = "SESSION_ID_USERDEFAULTS_KEY"
    static let WATCHLIST_USERDEFAULTS_KEY = "WATCHLIST_USERDEFAULTS_KEY"
    static let FAVORITE_USERDEFAULTS_KEY = "FAVORITE_USERDEFAULTS_KEY"
    static let TMDB_WATCHLIST_ID_KEY = "TMDB_WATCHLIST_ID_KEY"
    
    static let LOGIN_AUTH_ERROR = "Please fill username and password fields"

}
